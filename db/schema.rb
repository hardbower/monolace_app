# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160428024702) do

  create_table "entries", force: :cascade do |t|
    t.text     "title"
    t.text     "main_image"
    t.text     "photo_one"
    t.text     "photo_two"
    t.text     "photo_three"
    t.text     "photo_four"
    t.text     "para_one"
    t.text     "para_two"
    t.text     "para_three"
    t.text     "para_four"
    t.text     "blurb"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "title"
    t.string   "blurb"
    t.text     "main_image"
    t.text     "image_one"
    t.text     "image_two"
    t.text     "image_three"
    t.string   "desc_one"
    t.string   "desc_two"
    t.string   "desc_three"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
