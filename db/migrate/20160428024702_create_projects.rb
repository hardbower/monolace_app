class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.string :blurb
      t.text :main_image
      t.text :image_one
      t.text :image_two
      t.text :image_three
      t.string :desc_one
      t.string :desc_two
      t.string :desc_three

      t.timestamps null: false
    end
  end
end
