class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.text :title
      t.text :main_image
      t.text :photo_one
      t.text :photo_two
      t.text :photo_three
      t.text :photo_four
      t.text :para_one
      t.text :para_two
      t.text :para_three
      t.text :para_four
      t.text :blurb

      t.timestamps null: false
    end
  end
end
