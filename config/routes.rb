Rails.application.routes.draw do

  post   'post'   => 'entries#create'
  get 'blog'  => 'entries#index'
  get 'write' => 'entries#new'
  
  get 'portfolio' => 'projects#index'
  get 'create' => 'projects#new'
  post 'render' => 'projects#create'
  
  get 'sessions/new'

  root 'sessions#new'
  get 'signup' => 'users#new'
  get 'home' => 'static_pages#home'
  get 'admin' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :users
  resources :entries
  resources :projects

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

end
