class SessionsController < ApplicationController

  def new
    @users = User.all
  end

  def create
    store_location
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_back_or user
    else
      flash.now[:danger] = 'Incorrect credentials.'
      @users = User.all
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_back_or root_url
  end
end