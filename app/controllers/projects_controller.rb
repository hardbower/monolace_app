class ProjectsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :destroy]
  
  def index
    store_location
    if logged_in?
      @logged_in = true
    end
    @projects = Project.paginate(page: params[:page], :per_page => 8)
  end

  def show
    store_location
    if logged_in?
      @logged_in = true
    end
    @project = Project.find(params[:id])
  end

  def edit
    store_location
    @project = Project.find(params[:id])
  end

  def new
    store_location
    @project = Project.new
  end
  
  def create
    store_location
    @project = Project.new(project_params)
    if @project.save
      flash[:success] = "Project created successfully!"
      redirect_to portfolio_path
    else
      render 'new'
    end
  end
  
  def update
    @project = Project.find(params[:id])
    if @project.update_attributes(project_params)
      flash[:success] = "Project updated"
      redirect_to @project
    else
      render 'edit'
    end
  end
    
  def destroy
  end
    
    private

    def project_params
      params.require(:project).permit(:title, :main_image, :image_one, :image_two,
                                   :image_three, :desc_one, :desc_two,
                                   :desc_three, :blurb)
    end
end
