class EntriesController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :destroy]
  
  def index
    store_location
    if logged_in?
      @logged_in = true
    end
    @entries = Entry.paginate(page: params[:page], :per_page => 8)
  end

  def show
    store_location
    if logged_in?
      @logged_in = true
    end
    @entry = Entry.find(params[:id])
  end

  def edit
    store_location
    @entry = Entry.find(params[:id])
  end

  def new
    store_location
    @entry = Entry.new
  end
  
  def create
    @entry = Entry.new(entry_params)
    if @entry.save
      flash[:success] = "Entry posted successfully!"
      redirect_to blog_path
    else
      render 'new'
    end
  end
  
  def update
    @entry = Entry.find(params[:id])
    if @entry.update_attributes(entry_params)
      flash[:success] = "Entry updated"
      redirect_to @entry
    else
      render 'edit'
    end
  end
    
  def destroy
  end
    
    private

    def entry_params
      params.require(:entry).permit(:title, :main_image, :photo_one, :photo_two,
                                   :photo_three, :photo_four, :para_one,
                                   :para_two, :para_three, :para_four, :blurb)
    end
end
