class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :user_created, only: [:new]
  
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    if !User.exists?(1)
      @user = User.new(user_params)
      if @user.save
        log_in @user
        flash[:success] = "Welcome to Monolace!"
        redirect_to @user
      else
        render 'new'
      end
    else
      flash[:danger] = "Site has already been claimed."
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    
    # Before filters

    # Checks if user exists
    def user_created
      if User.exists?(1)
        flash[:danger] = "This site has already been claimed."
        redirect_to root_url
      end
    end
end