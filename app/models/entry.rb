class Entry < ActiveRecord::Base
  validates :title, presence: true, length: { maximum: 30 }
  validates :main_image, presence: true
end
